package Examen.Ejercicio1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map.Entry;

public class ExamenEjercicio1 {
	
	public static void main(String [] arg) {
	      File archivo = null;
	      FileReader fr = null;
	      BufferedReader br = null;

	      try {
	         // Apertura del fichero y creacion de BufferedReader para poder
	         // hacer una lectura comoda (disponer del metodo readLine()).
	         archivo = new File ("C:\\Users\\Sory\\git\\Examen_2_3_eva\\Examen_2_3_eva\\src\\Examen\\Ejercicio1\\compuestos");
	         fr = new FileReader (archivo);
	         br = new BufferedReader(fr);
	         HashMap<Integer, String> compuestos = new HashMap<Integer, String>();

	         // Lectura del fichero
	         String linea;
	         int k = 0;
	         while((linea=br.readLine())!=null)	        	 
	         {       	 
	        	 compuestos.put(k,linea);
	        	 k++;
	         }
	
	         
	         for(Entry<Integer, String> m :compuestos.entrySet()){
                 System.out.println(m.getValue());	//iterando por orden de lectura
             }
	         
	         for(Integer m :compuestos.keySet()){
                 System.out.println(m.intValue());	//iterando por orden de lectura
             }
	         
	         
	         
	      }  
	      catch(Exception e){
	         e.printStackTrace();
	      }finally{
	         // En el finally cerramos el fichero, para asegurarnos
	         // que se cierra tanto si todo va bien como si salta 
	         // una excepcion.
	         try{                    
	            if( null != fr ){   
	               fr.close();     
	            }                  
	         }catch (Exception e2){ 
	            e2.printStackTrace();
	         }
	      }
	         
	
}
	}
	      
	   
