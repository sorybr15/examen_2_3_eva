package Examen.Ejercicio2;

public class Asalariados extends Empleados {
	
	private String departamento;

	public Asalariados(String nombre, String apellidos, String fecha, int salario, String cuenta_bancaria,
			String departamento) {
		super(nombre, apellidos, fecha, salario, cuenta_bancaria);
		this.departamento = departamento;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

}
