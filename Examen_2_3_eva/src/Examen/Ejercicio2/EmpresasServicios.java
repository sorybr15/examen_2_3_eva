package Examen.Ejercicio2;

public class EmpresasServicios {

	private String nombre;
	private String tipoServicio;
	public int importe;
	public String cuentaBancaria;

	public EmpresasServicios(String nombre, String tipoServicio, int importe, String cuentaBancaria) {
		super();
		this.nombre = nombre;
		this.tipoServicio = tipoServicio;
		this.importe = importe;
		this.cuentaBancaria = cuentaBancaria;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTipoServicio() {
		return tipoServicio;
	}

	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}

	public int getImporte() {
		return importe;
	}

	public void setImporte(int importe) {
		this.importe = importe;
	}

	public String getCuentaBancaria() {
		return cuentaBancaria;
	}

	public void setCuentaBancaria(String cuentaBancaria) {
		this.cuentaBancaria = cuentaBancaria;
	}
}
